##  Daily Summary 

**Time: 2023/7/10**

**Author: 赖世龙**

---

#### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

**question 1:**

* Understood the overall learning schedule of bootcamp 
* Ice breaking games liven up the atmosphere, let me deepen the impression of the students, and let my interests show to the students 
* Through the short video played by the teacher, I realized the enthusiasm of independent learning and asked questions to better learn new knowledge 
* Through the teacher's lecture, I learned PDCA rules: plan, do, check, and adjust\act, which will be applied in my study and work 
* Understand the learning pyramid and know that active learning helps to deepen memory
* Understand what concept map is and how to use it, and brainstorm with the team to complete the concept map of what is computer 
* I learned about stand up meeting and the ORID rule to summarize and review the day

**question 2:**

* Ice breaking games
* Concept Map Practice

**question 3:**

*  When the team built the concept map, I was deeply impressed by everyone's active discussion 

#### R (Reflective):  Please use one word to express your feelings about today's class.

*  dynamic 

#### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

**question 1:**

* I think this activity is very interesting, and I have learned how to use concept map 

**question 2:**

*  enhanced the feelings of the group members

#### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

**question 1:**

*  In the process of learning React 

**question 2:**

*  Ask questions. Questions inspire interest. Interest is the best teacher 